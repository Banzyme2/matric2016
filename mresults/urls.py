from django.urls import path

from . import views

app_name = 'mresults'
urlpatterns = [
    path('', views.index, name='index'),
    path('trends/', views.trends, name='trends'),
    path('each/', views.each_school, name='each'),
    path('debug/', views.debg, name='debg'),
]
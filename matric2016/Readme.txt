This project has been created using Docker and Docker-compose

1. Install Docker and Dokcer Compose As follows

   $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    $  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    $ sudo apt-get update

    $  sudo apt-get install -y docker-ce

2. Next, Install Docker Compose

    $ sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

    $ sudo chmod +x /usr/local/bin/docker-compose

3. Create a working directory and clone from gitlab as follows:

    $ git clone git@gitlab.com:Banzyme2/matric2016.git

4. In the root directory(Where the Dockerfile is located), run:

    $ sudo docker-compose up --build
    $ Access the site on localhost:8000